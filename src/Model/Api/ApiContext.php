<?php

namespace App\Model\Api;


class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CLIENT = '/client';
    const ENDPOINT_CLIENT_PASSWORD_ENCODE = '/client/password/encode';
    const ENDPOINT_CONCRETE_CLIENT_BY_EMAIL = '/client/email/{email}';
    const ENDPOINT_CONCRETE_CLIENT = '/client/{passport}/{email}';
    const ENDPOINT_CHECK_CLIENT_CREDENTIALS = '/check_client_credentials/{encodedPassword}/{email}';
    const ENDPOINT_CONCRETE_CLIENT_SOCIAL = '/social/{uid}';
    const ENDPOINT_OBJECT_REGISTER = '/booking-object-register/{passport}';
    const ENDPOINT_OBJECT = '/get-booking-object/';
    const ENDPOINT_CONCRETE_OBJECT = '/get-concrete-booking-object/';
    const ENDPOINT_ONE_OBJECT = '/get-one-booking-object/';
    const ENDPOINT_BOOK_OBJECT = '/book-object/';

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @param string $passport
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT, [
            'passport' => $passport,
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $email
     * @return array
     * @throws ApiException
     */
    public function getClientByEmail(string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_EMAIL, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param string $plainPassword
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function checkClientCredentials(string $plainPassword, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_CREDENTIALS, [
            'encodedPassword' => $this->encodePassword($plainPassword),
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT, self::METHOD_POST, $data);
    }

    /**
     * @param string $password
     * @return mixed
     * @throws ApiException
     */
    public function encodePassword(string $password)
    {
        $response = $this->makeQuery(self::ENDPOINT_CLIENT_PASSWORD_ENCODE, self::METHOD_GET, [
            'plainPassword' => $password
        ]);

        return $response['result'];
    }

    /**
     * @param $main
     * @param $dop
     * @param $passport
     * @return mixed
     * @throws ApiException
     */
    public function registerObject($main, $dop, $passport)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_OBJECT_REGISTER, [
            'passport' => $passport
        ]);
        $response = $this->makeQuery($endPoint, self::METHOD_POST, [
            'main' => $main,
            'dop' => $dop
        ]);
        return $response['result'];
    }

    /**
     * @param string $uid
     * @return array
     * @throws ApiException
     */
    public function getClientIfExistsSocial(string $uid)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_SOCIAL, [
            'uid' => $uid
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }
    /**
     * @return array
     * @throws ApiException
     */
    public function getBookingObject()
    {
        return $this->makeQuery(self::ENDPOINT_OBJECT, self::METHOD_GET);
    }

    /**
     * @param $data
     * @return array
     * @throws ApiException
     */
    public function getConcreteBookingObject($data)
    {
        return $this->makeQuery(self::ENDPOINT_CONCRETE_OBJECT, self::METHOD_GET, $data);
    }

    /**
     * @param $name
     * @return array
     * @throws ApiException
     */
    public function getOneBookingObject($name)
    {
        return $this->makeQuery(self::ENDPOINT_ONE_OBJECT, self::METHOD_GET, $name);
    }

    /**
     * @param $data
     * @return array
     * @throws ApiException
     */
    public function makeBookingObject($data)
    {
        return $this->makeQuery(self::ENDPOINT_BOOK_OBJECT, self::METHOD_POST, $data);
    }
}
