<?php
/**
 * Created by PhpStorm.
 * User: timur
 * Date: 06.07.18
 * Time: 17:39
 */

namespace App\Model\Booking;


use App\Entity\Cottage;
use App\Entity\Pension;

class BookingHandler
{
    /**
     * @param $objects
     * @return array
     */
    public function arrayToObject($objects){
        $bookingObjects = [];
        foreach ($objects as $object){
            if($object['type'] === 'cottage'){
                $cottage = new Cottage();
                $this->object($cottage, $object);
                $cottage->setType($object['type']);
                $cottage->setFireplace($object['fireplace']);
                $cottage->setMaid($object['maid']);
                $bookingObjects[] = $cottage;
            }else{
                $pension = new Pension();
                $this->object($pension, $object);
                $pension->setType($object['type']);
                $pension->setFood($object['food']);
                $pension->setMiniBar($object['minibar']);
                $bookingObjects[] = $pension;
            }
        }
        return $bookingObjects;
    }
    /**
     * @param $booking
     * @param $object
     */
    public function object($booking, $object)
    {
        $booking->setObjectName($object['objectName']);
        $booking->setRoomNumber($object['roomNumber']);
        $booking->setContactPerson($object['contactPerson']);
        $booking->setContactPhone($object['contactPhone']);
        $booking->setAddress($object['address']);
        $booking->setPrice($object['price']);
    }
}