<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('search', TextType::class, [
                'required' => false
            ])
            ->add('min', IntegerType::class, [
                'required' => false
            ])
            ->add('max', IntegerType::class, [
                'required' => false
            ])
            ->add('type', ChoiceType::class, [
                'required' => false,
                'label' => 'Вид',
                'placeholder' => "тип",
                'choices' => [
                    'коттедж' => 'cottage',
                    'пансион' => 'pension'
                ]
            ])
            ->add('Поиск', SubmitType::class)
        ;
    }
}
