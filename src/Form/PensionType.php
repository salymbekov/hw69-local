<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class PensionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('services', ChoiceType::class, [
                'label' => 'дополнительные фишки',
                'expanded'=> true,
                "multiple"=> true,
                'choices' => [
                    'еда' => 'food',
                    'мини-бар' => 'bar'
                ]
            ])
            ->add('Зарегистрировать', SubmitType::class)
        ;
    }
}
