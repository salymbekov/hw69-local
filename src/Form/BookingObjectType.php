<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class BookingObjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('objectType', ChoiceType::class, [
                'label' => 'Тип обьекта',
                'placeholder' => "Выберите ваш тип",
                'choices' => [
                    'котедж' => 'cottage',
                    'панисионат' => 'pension'
                ]
            ])
            ->add(
                'name',
                TextType::class
            )
            ->add(
                'roomNumber',
                TextType::class
            )
            ->add(
                'contactPerson',
                TextType::class
            )
            ->add(
                'contactPhone',
                TelType::class
            )
            ->add(
                'address',
                TextType::class,
                [
                    'attr'=> [
                        'id'=>'address'
                    ]
                ]
            )
            ->add(
                'price',
                TextType::class
            )
            ->add('submit', SubmitType::class,[
                'label'=> 'Далее'
            ])
        ;
    }
}
