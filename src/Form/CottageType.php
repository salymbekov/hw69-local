<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class CottageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('services', ChoiceType::class, [
                'label' => 'дополнительные фишки',
                'expanded'=> true,
                'multiple'=> true,
                'choices' => [
                    'камин' => 'fireplace',
                    'горничная' => 'maid'
                ]
            ])
            ->add('Зарегистрировать', SubmitType::class)
        ;
    }
}
