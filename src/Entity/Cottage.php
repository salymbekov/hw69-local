<?php

namespace App\Entity;


class Cottage extends BookingObject
{

    private $fireplace;

    private $maid;

    public function setFireplace($fireplace)
    {
        $this->fireplace = $fireplace;
        return $this;
    }

    public function getFireplace()
    {
        return $this->fireplace;
    }


    public function setMaid($maid)
    {
        $this->maid = $maid;
        return $this;
    }


    public function getMaid()
    {
        return $this->maid;
    }

    public function toArray()
    {
        $result = $this->__toArray();
        $result['fireplace'] = $this->fireplace;
        $result['maid'] = $this->maid;
        return $result;
    }

}