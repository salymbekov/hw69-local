<?php

namespace App\Entity;

use App\Model\User\UserHandler;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("email")
 * @UniqueEntity("passport")
 */
class User implements UserInterface
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024)
     */
    private $roles;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $passport;

    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $email;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $registeredAt;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $vkontakte;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     */
    private $google;

    public function __construct()
    {
        $this->registeredAt = new \DateTime("now");
        $this->roles = json_encode(['ROLE_USER']);
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredAt(): ?\DateTime
    {
        return $this->registeredAt;
    }

    /**
     * @param \DateTime $registeredAt
     */
    public function setRegisteredAt(\DateTime $registeredAt): void
    {
        $this->registeredAt = $registeredAt;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return json_decode($this->roles, true);
    }

    public function addRole(string $role)
    {
        $roles = json_decode($this->roles, true);
        $roles[] = $role;
        $this->roles = json_encode($roles);
    }

    public function setRoles(array $roles)
    {
        $this->roles = json_encode($roles);
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->getEmail();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    /**
     * @param mixed $username
     * @return User
     */
    public function setUsername($username)
    {
        return $this->setEmail($username);
    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $passport
     * @return User
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param mixed $vkontakte
     * @return User
     */
    public function setVkontakte($vkontakte)
    {
        $this->vkontakte = $vkontakte;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVkontakte()
    {
        return $this->vkontakte;
    }

    /**
     * @param mixed $facebook
     * @return User
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param mixed $google
     * @return User
     */
    public function setGoogle($google)
    {
        $this->google = $google;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoogle()
    {
        return $this->google;
    }

    public function setSocialId($network, $id) {
        switch($network) {
            case UserHandler::SOC_NETWORK_VKONTAKTE:
                $this->vkontakte = $id;
                break;
            case UserHandler::SOC_NETWORK_FACEBOOK:
                $this->facebook = $id;
                break;
            case UserHandler::SOC_NETWORK_GOOGLE:
                $this->google = $id;
                break;
        }
        return $this;
    }

    public function getSocialId($network) {
        switch($network) {
            case UserHandler::SOC_NETWORK_VKONTAKTE:
                return $this->vkontakte;
                break;
            case UserHandler::SOC_NETWORK_FACEBOOK:
                return $this->facebook;
                break;
            case UserHandler::SOC_NETWORK_GOOGLE:
                return $this->google;
                break;
            default:
                return null;
        }
    }

    public function __toArray() {
        return [
            'email' => $this->email,
            'password' => $this->password,
            'passport' => $this->passport,
            'vkId' => $this->vkontakte,
            'faceBookId' => $this->facebook,
            'googleId' => $this->google,
        ];
    }
}
