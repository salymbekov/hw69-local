<?php


namespace App\Entity;

class Pension extends BookingObject
{

    private $food;

    private $miniBar;

    public function setFood($food)
    {
        $this->food = $food;
        return $this;
    }

    public function getFood()
    {
        return $this->food;
    }

    public function setMiniBar($miniBar)
    {
        $this->miniBar = $miniBar;
        return $this;
    }

    public function getMiniBar()
    {
        return $this->miniBar;
    }

    public function toArray(){
        $result = $this->__toArray();
        $result['minibar'] = $this->miniBar;
        $result['food'] = $this->food;
        return $result;
    }
}