<?php

namespace App\Entity;


class BookingObject
{

    private $id;

    private $objectName;

    private $roomNumber;

    private $contactPerson;

    private $contactPhone;

    private $address;

    private $price;

    private $owner;

    private $type;

    public function setObjectName($objectName)
    {
        $this->objectName = $objectName;
        return $this;
    }

    public function getObjectName()
    {
        return $this->objectName;
    }

    public function setRoomNumber($roomNumber)
    {
        $this->roomNumber = $roomNumber;
        return $this;
    }

    public function getRoomNumber()
    {
        return $this->roomNumber;
    }

    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;
        return $this;
    }

    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;
        return $this;
    }

    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function __toArray() {
        return [
            'objectName' => $this->objectName,
            'roomNumber' => $this->roomNumber,
            'contactPerson' => $this->contactPerson,
            'contactPhone' => $this->contactPhone,
            'address' => $this->address,
            'price' => $this->price
        ];
    }

    /**
     * @param mixed $type
     * @return BookingObject
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }
}