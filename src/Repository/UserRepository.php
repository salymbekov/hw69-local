<?php

namespace App\Repository;

use App\Entity\User;
use App\Model\User\UserHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(
        RegistryInterface $registry,
        UserHandler $userHandler)
    {
        parent::__construct($registry, User::class);
        $this->userHandler = $userHandler;
    }

    /**
     * @param string $plainPassword
     * @param string $email
     * @return User|null
     */
    public function getByCredentials(string $plainPassword, string $email)
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.password = :password')
                ->andWhere('a.email = :email')
                ->setParameter(
                    'password',
                    $this->userHandler->encodePlainPassword($plainPassword)
                )
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param  $uid
     *
     * @param $field
     * @return User|null
     */
    public function getByUid($uid, $field)
    {

        try {
            return $this->createQueryBuilder('u')
                ->select('u')
                ->where('u.facebook = :uid')
                ->orWhere('u.google = :uid')
                ->orWhere('u.vkontakte = :uid')
                ->setParameter('uid', $uid)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param $email
     * @param $passport
     * @return User|null
     */
    public function getByEmailAndPassport($email, $passport)
    {

        try {
            return $this->createQueryBuilder('u')
                ->select('u')
                ->where('u.email = :email')
                ->andWhere('u.passport = :passport')
                ->setParameter('email', $email)
                ->setParameter('passport', $passport)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
