<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 19.06.18
 * Time: 19:48
 */

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class uLoginController extends Controller
{
    const ULOGIN_DATA = 'ulogin-data';

    /**
     * @Route("/register-case", name="app-register-case")
     */
    public function registerCaseAction()
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = new User();
        $user->setEmail($userData['email']);

        $form = $this->createForm(
            'App\Form\ULoginRegisterType', $user, [
                'action' => $this
                    ->get('router')
                    ->generate('app-register-case2')
            ]
        );
        //$user['network'] - соц. сеть, через которую авторизовался пользователь
        //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
        //$user['first_name'] - имя пользователя
        //$user['last_name'] - фамилия пользователя
        $this->get('session')->set(self::ULOGIN_DATA, $s);

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => null
        ]);
    }

    /**
     * @Route("/register-case2", name="app-register-case2")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerCase2Action(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    ) {
        $userData = json_decode(
            $this
                ->get('session')
                ->get(self::ULOGIN_DATA),
            true
        );

        $user = new User();

        $form = $this->createForm(
            'App\Form\ULoginRegisterType',
            $user
        );

        $form->handleRequest($request);

        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $user->setPassword("social------------------------" . time());
                    $user->setSocialId($userData['network'], $userData['uid']);
                    $data = $user->__toArray();

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);

                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("home-page");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/social-link", name="social-link")
     * @param ObjectManager $objectManager
     * @return Response
     */
    public function socialAction(ObjectManager $objectManager)
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $user_data = json_decode($s, true);
        //$user['network'] - соц. сеть, через которую авторизовался пользователь
        //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
        //$user['first_name'] - имя пользователя
        /**
         * @var User $user
         */
        $user = $this->getUser();
        $user->setSocialId($user_data['network'], $user_data['uid']);
        $objectManager->flush();

        return new Response('Вы успешно привязали соц сеть к вашему аккаунту');
    }

    /**
     * @Route("/social-auth", name="/social-auth")
     * @param UserRepository $userRepository
     * @param UserHandler $userHandler
     *
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return Response
     * @throws ApiException
     */
    public function socialAuthAction(
        UserRepository $userRepository,
        UserHandler $userHandler,
        ApiContext $apiContext,
        ObjectManager $manager
    ) {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $user_data = json_decode($s, true);
        $user = $userRepository->getByUid($user_data['uid'], $user_data['network']);
        if ($user) {
            $userHandler->makeUserSession($user);
            return $this->redirectToRoute('home-page');
        } else {
            $centralClient = $apiContext->getClientIfExistsSocial($user_data['uid']);
            if ($centralClient) {
                /**
                 * @var User $user
                 */
                $user = $userRepository->getByEmailAndPassport($centralClient['email'], $centralClient['passport']);
                if ($user) {
                    $user->setFacebook($centralClient['facebook']);
                    $user->setVkontakte($centralClient['vkontakte']);
                    $user->setGoogle($centralClient['google']);
                    $manager->flush();
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('home-page');
                }else{
                    $user = $userHandler->createNewUser($centralClient, false);
                    $manager->flush();
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('home-page');
                }
            }else{
                return new Response('Вы еще не регистрировались у нас, либо не привязали данную соц сеть к аккаунту');
            }
        }
    }
}