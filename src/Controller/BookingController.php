<?php

namespace App\Controller;

use App\Model\Api\ApiContext;
use App\Model\Booking\BookingHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BookingController extends Controller
{
    /**
     * @Route("/object", name="app-object")
     * @param Request $request
     * @return Response
     */
    public function objectRegisterAction(
        Request $request
    ) {
        $form = $this->createForm("App\Form\BookingObjectType");

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            return $this->redirectToRoute('app-object2', ['object' => $data]);
        }

        return $this->render('objectRefister.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/object2", name="app-object2")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return Response
     * @throws \App\Model\Api\ApiException
     */
    public function objectRegister2Action(
        Request $request,
        ApiContext $apiContext
    ) {
        $object = $request->query->get('object');
        if ($object['objectType'] === 'pension') {
            $form = $this->createForm("App\Form\PensionType");
        } else {
            $form = $this->createForm("App\Form\CottageType");
        }
        $passport = $this->getUser()->getPassport();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $result = $apiContext->registerObject($object, $data, $passport);
            return $this->redirectToRoute('home-page');
        }

        return $this->render('cottage.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/object-filter", name="app-object-filter")
     * @param Request $request
     * @param ApiContext $apiContext
     * @param BookingHandler $bookingHandler
     * @return Response
     * @throws \App\Model\Api\ApiException
     */
    public function objectFilterAction(
        Request $request,
        ApiContext $apiContext,
        BookingHandler $bookingHandler
    ) {
        $result = null;
        $form = $this->createForm("App\Form\FilterType", null, [
            'action' => $this->generateUrl('app-object-filter')
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            if ($data['search'] == null && $data['min'] == null
                && $data['max'] == null && $data['type'] == null) {
                return $this->redirectToRoute('home-page');
            } else {
                $objects = $apiContext->getConcreteBookingObject($data);
                $result = $bookingHandler->arrayToObject($objects['result']);
            }

        }
        return $this->render('home.html.twig', [
            'form' => $form->createView(),
            'objects' => $result
        ]);
    }

    /**
     * @Route("/object-detail/{name}", name="object-detail")
     * @param string $name
     * @param ApiContext $apiContext
     * @param BookingHandler $bookingHandler
     * @return Response
     * @throws \App\Model\Api\ApiException
     */
    public function objectDetailAction(
        string $name,
        ApiContext $apiContext,
        BookingHandler $bookingHandler
    )
    {
        $results = $apiContext->getOneBookingObject(['search' => $name]);
        $object = $bookingHandler->arrayToObject($results['result']);
        $booked = [];
        foreach ($results['booked'] as $result){
            $booked[] = (object)$result;
        }
        return $this->render('objectDetail.html.twig', [
            'object' => $object[0],
            'dates' => $booked
        ]);
    }

    /**
     * @Route("/object-booking/{objectName}/{number}", name="object-booking")
     * @param string $objectName
     * @param string $number
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \App\Model\Api\ApiException
     */
    public function objectBookingAction(
        string $objectName,
        string $number,
        ApiContext $apiContext
    )
    {
        $result = $apiContext->makeBookingObject(['objectName'=> $objectName, 'room'=> $number, 'tenant'=> $this->getUser()->getEmail()]);
        return $this->redirectToRoute('object-detail', ['name'=> $objectName]);
    }


}
