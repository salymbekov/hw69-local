<?php

use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends AttractorContext
{
    /**
     * @When /^я вижу слово "([^"]*)" на странице$/
     * @param $arg1
     */
    public function яВижуСловоГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit('/');
    }

    /**
     * @When /^я перехожу на страницу "([^"]*)"$/
     */
    public function яПерехожуНаСтраницу($page)
    {
        $this->clickLink($page);
    }

    /**
     * @When /^я прохожу авторизацию$/
     */
    public function яПрохожуАвторизацию()
    {
        $this->fillField('auth_email', '2@2.2');
        $this->fillField('auth_password', '123');
        $this->pressButton('auth_login');
    }

    /**
     * @When /^заполняю поля регистрации$/
     * @param \Behat\Gherkin\Node\TableNode $table
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function заполняюПоляРегистрации(TableNode $table)
    {
        $this->fillFields($table);
        $this->getSession()->getPage()->selectFieldOption('register_roles', 'Арендодатель');
        $this->pressButton('register_save');

    }

    /**
     * @When /^заполняю поля регистрации нового обьекта$/
     * @param \Behat\Gherkin\Node\TableNode $table
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function заполняюПоляРегистрацииНовогоОбьекта(TableNode $table)
    {
        $script = 'document.elementFromPoint(100, 100).click()';
        $this->fillFields($table);
        $this->getSession()->getPage()->selectFieldOption('booking_object_objectType', 'котедж');
        $this->getSession()->executeScript($script);
        sleep(3);
        $this->pressButton('booking_object_submit');
        sleep(3);

    }

    /**
     * @When /^выбераю доп параметры$/
     */
    public function выбераюДопПараметры()
    {
        $this->checkOption('cottage_services_1');
        sleep(3);
        $this->pressButton('cottage_Зарегистрировать');
        sleep(3);
    }
}